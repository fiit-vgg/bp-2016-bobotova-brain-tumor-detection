# README #

BP 2016 - Bobotova - Brain tumor detection 

Author/Authors:Zuzana Bobotova

Bachelor thesis: Image processing and computer vision methods applied for medical data

Supervisor: Ing. Vanda Benesova, PhD.

Keywords: MRI, tumor, brain, adaptive thresholding, Mixture of Gaussians, adaptive greyscale morphological operation, graph cut segmentation

Year and month of termination: May 2016

Thesis Description:

Manual processing of medical data is time consuming task. Hence, computer vision can be used to save doctor�s and diagnostic�s time. Different methods are suitable for different specific tasks of image processing. Automatic method for brain tumor segmentation from magnetic resonance images is presented in this work. It consists of three steps. First, contrast of the image is enhanced. In the next step, cranium is removed from the image to prevent error segmentations caused by the cranium. Finally the tumor is segmented using adaptive methods. Parameters for adaptive methods are counted by statistical method Mixture of Gaussians. Result of adaptive methods is first tumor segmentation. Then the graph cut algorithm is used to improve precision of segmentation. Methods are tested on the 500 randomly selected 2D images from the practice and also on the 500 randomly selected 2D images from BRATS dataset. Resulting segmentations are compared to manual segmentations provided by the experts.

Video about the work: https://www.youtube.com/watch?v=_fet7h3KdtI